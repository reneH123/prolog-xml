package transform;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import alice.tuprolog.InvalidLibraryException;
import alice.tuprolog.InvalidTheoryException;
import alice.tuprolog.MalformedGoalException;
import alice.tuprolog.NoSolutionException;
import alice.tuprolog.Prolog;
import alice.tuprolog.SolveInfo;
import alice.tuprolog.Term;
import alice.tuprolog.Theory;

public class Main {
	private SolveInfo result;
	private TransformLibrary library;
	private Prolog engine;

	public Main() {
		this.engine = new Prolog();
		this.library = new TransformLibrary();
		try {
			// load library with built-in predicates
			this.engine.loadLibrary(library);
		} catch (InvalidLibraryException e) {
			System.err.println("Error: could not load library !");
			e.printStackTrace();
			System.exit(88);
		}
	}

	public void setTheory(String theory) {
		try {
			Theory th = new Theory(theory);
			engine.setTheory(th);
		} catch (InvalidTheoryException e) {
			System.err.println("Error: invalid theory set at line " + e.line + " pos " + e.pos + " with message "
					+ e.getMessage());
			System.out.println(">>>\n" + theory + "\n<<<");
			System.exit(99);
		}
	}

	// readProgramFromFile("prolog/checkasserts.pl") only allowed
	// reads the Prolog rules in order to perform a check over all assertions
	public static String readProgramFromFile(String fileName, String fileExtension) {
		// first check for check file extension
		int lastIndexOf = fileName.lastIndexOf(".");
		System.out.println("DEBUG: reading file " + fileName + " with extension " + fileName.substring(lastIndexOf));
		if (lastIndexOf == -1 || fileName.substring(lastIndexOf).equals(fileExtension) == false) {
			System.err.print("Error: file needs to have extension " + fileExtension + "!");
			System.exit(3);
		}

		// read from file (if available)
		String program = null;
		File file = new File(fileName);
		FileReader fileReader = null;
		try {
			fileReader = new FileReader(file);
			char[] chars = new char[(int) file.length()];
			fileReader.read(chars);
			program = new String(chars);
			fileReader.close();
		} catch (IOException e) {
			System.err.println("Error: Could not open file '" + fileName + "' !");
			System.exit(50);
		}
		return program;
	}

	public void executeGoal(String goals) {
		try {
			System.out.println("DEBUG Execute GOAL: " + goals);
			this.result = this.engine.solve(goals);
		} catch (MalformedGoalException e) {
			System.out.println("Used Theory:\n>>>\n" + this.library.getTheory() + "\n<<<");
			System.err.println("Error: Malformed goal: '" + goals + "' !");
			System.exit(60);
		}
	}

	public Term getResult() {
		try {
			if (this.result == null) {
				System.err.println("Error: no result yet available (maybe you have forgotten to call a goal)!");
				System.exit(71);
				return null;
			}
			return this.result.getSolution();
		} catch (NoSolutionException e) {
			System.out.println("Used Theory:\n>>>\n" + this.library.getTheory() + "\n<<<");
			System.err.println("Error: No solution available!");
			System.exit(72);
			return null;
		}
	}

	public static void main(String[] args) {
		// (1) check program switches
		if (args.length != 3) {
			System.out.println("Usage :> java xtl.jar INPUT.xml SCRIPT.pl OUTPUT.XML");
			System.exit(-1);
		}
		System.out.println("DEBUG: XTL " + args[0] + " " + args[1] + " " + args[2]);

		// (2) check input exists
		String inFileXML = args[0];
		String transformationFileProlog = args[1];
		String outFileXML = args[2];

		Main.readProgramFromFile(inFileXML, ".xml"); // result is not needed, however, checks still are required 
		String transformationProlog = Main.readProgramFromFile(transformationFileProlog, ".pl");

		String operatorsProlog = Main.readProgramFromFile("operators.pl", ".pl");
		String incomingFilePredicate = "incoming('" + inFileXML + "').";
		String outgoingFilePredicate = "outgoing('" + outFileXML + "').";

		// (3) set theory and initialise library
		Main m = new Main();

		String theory = incomingFilePredicate + "\n" + outgoingFilePredicate + "\n\n" + operatorsProlog + "\n\n"
				+ transformationProlog;
		m.setTheory(theory);

		// (4) invoke checking-predicate with incoming program as input parameter
		m.executeGoal("go(XIN,XOUT).");

		// (5) check there are results available
		Term term_res = m.getResult(); // we are only interested in the first solution (more solutions are strictly speaking incorrect due to well-formedness)
		System.out.println("DEBUG: outgoing XML as Prolog term: " + term_res.getTerm().toString());

		System.out.println("...ready!");
	}

}
