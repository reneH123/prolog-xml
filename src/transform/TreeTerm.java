// Author: René Haberland
// 30.12.2018, Saint Petersburg, Russia
//  licensed under Creative Commons 4.0 Share Alike (CC BY-SA 4.0)
//  For more informations on the license terms, visit https://creativecommons.org
package transform;

import java.util.Iterator;

import alice.tuprolog.Double;
import alice.tuprolog.Int;
import alice.tuprolog.Struct;
import alice.tuprolog.Term;
import alice.tuprolog.Var;

public class TreeTerm {
	private Term tree;

	public TreeTerm() {
		tree = null;
	}

	public TreeTerm(Term tree) {
		this.tree = tree;
	}

	private Double extractNumber(Term t1) {
		Var C1 = new Var();
		Var text1 = new Var();

		if (t1.getTerm().unify(new Struct("element", new Var(), new Var(), C1)) == false)
			return null;
		if ((((Struct) C1.getTerm()).listHead()).getTerm().unify(
				new Struct("text", text1)) == false)
			return null;
		String s1 = unbracket(text1.getTerm().toString());

		@SuppressWarnings( "deprecation" )
		Double number = new Double((new java.lang.Double(s1)).doubleValue());
		return number;
	}

	private String unbracket(String in) {
		String out = in;
		if (in.charAt(0) == '\'') {
			out = in.substring(1, in.length() - 1);
		}
		return out;
	}

	public Term position(Term child) {
		Term result = null;
		Var C = new Var();
		if (tree.getTerm()
				.unify(new Struct("element", new Var(), new Var(), C))) {

			@SuppressWarnings("rawtypes")
			Iterator it = ((Struct) C.getTerm()).listIterator();

			for (int i = 0; it.hasNext(); i++) {
				if (((Struct) it.next()).match(child)) {
					result = new Int(++i);
					break;
				}
			}
		}
		return result;
	}

	public Term plus(Term t) {
		Double d1 = extractNumber(t);
		Double d2 = extractNumber(tree);

		if ((d1 == null) || (d2 == null)) {
			return null;
		}
		double f1 = d1.doubleValue();
		double f2 = d2.doubleValue();
		int abs = (int) (f1 + f2);

		if ((f1 + f2) == (double) abs) {
			@SuppressWarnings( "deprecation" )
			Integer i = new Integer(abs);
			return new Struct(i.toString());
		} else {
			Double f = new Double(f1 + f2);
			return new Struct(f.toString());
		}
	}

	public Term minus(Term t) {
		Double d1 = extractNumber(t);
		Double d2 = extractNumber(tree);

		if ((d1 == null) || (d2 == null)) {
			return null;
		}
		double f1 = d1.doubleValue();
		double f2 = d2.doubleValue();
		int abs = (int) (f1 - f2);

		if ((f1 - f2) == (double) abs) {
			@SuppressWarnings( "deprecation" )
			Integer i = new Integer(abs);
			return new Struct(i.toString());
		} else {
			Double f = new Double(f1 - f2);
			return new Struct(f.toString());
		}
	}

	public Term mult(Term t) {
		Double d1 = extractNumber(t);
		Double d2 = extractNumber(tree);

		if ((d1 == null) || (d2 == null)) {
			return null;
		}
		double f1 = d1.doubleValue();
		double f2 = d2.doubleValue();
		long abs = (long) (f1 * f2);

		if (((double) ((int) f1) == f1) && ((double) ((int) f2) == f2)) {
			@SuppressWarnings( "deprecation" )
			Long l = new Long(abs);
			return new Struct(l.toString());
		} else {
			Double f = new Double(f1 * f2);
			return new Struct(f.toString());
		}
	}

	public Term div(Term t) {
		Double d1 = extractNumber(t);
		Double d2 = extractNumber(tree);

		if ((d1 == null) || (d2 == null)) {
			return null;
		}
		double f1 = d1.doubleValue();
		double f2 = d2.doubleValue();

		if (f2 == 0) {
			if (f1 == 0) {
				return new Struct("NaN");
			} else if (f1 < 0) {
				return new Struct("-Infinity");
			} else {
				return new Struct("Infinity");
			}
		} else {
			long f3 = (long) (f1 / f2);
			if (f1 / f2 == (double) f3) {
				@SuppressWarnings( "deprecation" )
				Long l = new Long(f3);
				return new Struct(l.toString());
			} else {
				Double f4 = new Double(f1 / f2);
				return new Struct(f4.toString());
			}
		}
	}
}
