// Author: Rene Haberland
// 30.12.2018, Saint Petersburg, Russia
//  licensed under Creative Commons 4.0 Share Alike (CC BY-SA 4.0)
//  For more informations on the license terms, visit https://creativecommons.org
package transform;

import java.io.File;
import java.util.Iterator;

import alice.tuprolog.Int;
import alice.tuprolog.InvalidTermException;
import alice.tuprolog.Library;
import alice.tuprolog.Number;
import alice.tuprolog.Struct;
import alice.tuprolog.Term;
import serializer.CanonicalVisitor;
import serializer.Consumer;
import serializer.ConsumerVirtual;
import serializer.PrologTermNode;
import serializer.XMLVisitor;

public class TransformLibrary extends Library {
	/**
	 * 
	 * @param fileName
	 *            file name of the XML document
	 * @param term
	 *            Prolog-representation of the XML-document name 'fileName'
	 * @return true, if 'term' equals the Prolog-representation of the XML-document
	 *         named 'fileName' false, otherwise
	 */
	public boolean parse2_2(Term fileName, Term term) {
		try {
			// parse2(fileName,term?) ~ parse
			if (term.isVar()) {
				if (fileName.isVar())
					return false;
				// generate term and unify with 'term'
				String s = ((Struct) fileName).toString();
				s = s.substring(1, s.length() - 1); // skip single quotes from atom

				// check again file exists, otherwise fail here
				{
					File f = new File(s);
					if (!f.exists()) {
						System.err.println("FAIL file '" + fileName + "' not found in parse2_2 !");
						return false;
					}
				}

				serializer.Parser parser = new serializer.Parser(s);
				Consumer consumer = new ConsumerVirtual();
				parser.execute(consumer);
				String unparsedTerm = (String) consumer.getLast();
				term.unify(Term.parse(unparsedTerm));
			}
			// parse2(fileName?,term) or parse2(fileName,term) ~ serialize
			else {
				String s = ((Struct) fileName).toString();
				s = s.substring(1, s.length() - 1); // skip single quotes from atom

				XMLVisitor xmlVisitor = new XMLVisitor(s);
				PrologTermNode myNode = new PrologTermNode();
				myNode.setStructReference((Struct) term.copy());
				myNode.accept(xmlVisitor);
				String fName = xmlVisitor.getTargetFile();
				int i = fName.indexOf('.');
				fName = fName.substring(0, i);
				fileName.unify(new Struct(fName));
				return true;
			}
			return true;
		} catch (InvalidTermException e) {
			e.printStackTrace();
		} catch (ClassCastException e) {
			e.printStackTrace();
		}
		return false;
	}

	public boolean canon_2(Term canoned, Term uncanoned) {
		try {
			CanonicalVisitor canVisitor = new CanonicalVisitor();
			PrologTermNode myNode = new PrologTermNode();
			myNode.setStructReference((Struct) uncanoned);
			myNode.accept(canVisitor);
			return canoned.unify(canVisitor.getCanonicalTerm().getStructReference());
		} catch (Exception e) {
			e.printStackTrace();
		}
		return false;
	}

	private String unbracket(String in) {
		String out = in;
		if (in.charAt(0) == '\'') {
			out = in.substring(1, in.length() - 1);
		}
		return out;
	}

	public Term cat_2(Term t1, Term t2) {
		Term result = null;
		if ((t1 == null) || (t2 == null))
			return null;
		if ((t1.isGround() == false) || (t2.isGround() == false))
			return null;
		if ((t1.isList()) || (t2.isList()))
			return null;
		if ((t1.isVar()) || (t2.isVar()))
			return null;
		if (t1.isAtom() && t2.isAtom()) {
			String s1 = t1.getTerm().toString();
			s1 = unbracket(s1);
			String s2 = t2.getTerm().toString();
			s2 = unbracket(s2);
			result = new Struct(s1.concat(s2));
		}
		return result;
	}

	public Term cat_3(Term t1, Term t2, Term t3) {
		return cat_2(cat_2(t1, t2), t3);
	}

	public Term cat_4(Term t1, Term t2, Term t3, Term t4) {
		return cat_2(cat_2(t1, t2), cat_2(t3, t4));
	}

	public Term cat_5(Term t1, Term t2, Term t3, Term t4, Term t5) {
		return cat_2(cat_3(t1, t2, t3), cat_2(t4, t5));
	}

	public Term cat_6(Term t1, Term t2, Term t3, Term t4, Term t5, Term t6) {
		return cat_2(cat_3(t1, t2, t3), cat_3(t4, t5, t6));
	}

	public Term cat_7(Term t1, Term t2, Term t3, Term t4, Term t5, Term t6, Term t7) {
		return cat_2(cat_4(t1, t2, t3, t4), cat_3(t5, t6, t7));
	}

	public Term cat_8(Term t1, Term t2, Term t3, Term t4, Term t5, Term t6, Term t7, Term t8) {
		return cat_2(cat_4(t1, t2, t3, t4), cat_4(t5, t6, t7, t8));
	}

	/**
	 * 
	 * @param terms,
	 *            a list of strings to be concatenated
	 * @return a string
	 */
	public Term cat_1(Term terms) {
		Term result = null;
		if (terms.isStruct() == false)
			return null;
		Iterator it = ((Struct) terms.getTerm()).listIterator();
		if (it.hasNext()) {
			result = (Struct) it.next();
		} else {
			return null;
		}
		for (; it.hasNext();) {
			Struct struct = (Struct) it.next();
			result = cat_2(result, struct);
		}
		return result;
	}

	/**
	 * 
	 * @param t1
	 *            input string, e.g. '-100.32'
	 * @return number, e.g. -100.32 interpreted as Float
	 */
	public Term fnumber_1(Term t1) {
		ArithmeticTerm aTerm = new ArithmeticTerm(t1);
		return aTerm.fnumber();
	}

	/**
	 * 
	 * @param t1
	 *            input string, e.g. '100'
	 * @return number, e.g. 100 interpreted as integer
	 */
	public Term inumber_1(Term t1) {
		ArithmeticTerm aTerm = new ArithmeticTerm(t1);
		return aTerm.inumber();
	}

	/**
	 * 
	 * @param t1
	 *            number, e.g. 100
	 * @return string representation of the number, e.g. '100'
	 */
	public Term string_1(Term t1) {
		String s = t1.getTerm().toString();
		s = unbracket(s);
		Term result = new Struct(s);
		return result;
	}

	/**
	 * 
	 * @param t1
	 *            some string
	 * @param t2
	 *            some string (it is asserted that t2 is prefix of t1)
	 * @return true, if string t1 starts with substring t2 false, otherwise
	 */
	public boolean starts_with_2(Term t1, Term t2) {
		String s1 = t1.getTerm().toString();
		s1 = unbracket(s1);
		String s2 = t2.getTerm().toString();
		s2 = unbracket(s2);
		return s1.startsWith(s2);
	}

	/**
	 * 
	 * @param t1
	 *            some string
	 * @param t2
	 *            some string
	 * @return true, if String t1 contains substring t2 false, otherwise
	 */
	public boolean contains_2(Term t1, Term t2) {
		if ((t1.isAtom() == false) || (t2.isAtom() == false))
			return false;
		String s1 = t1.getTerm().toString();
		s1 = unbracket(s1);
		String s2 = t2.getTerm().toString();
		s2 = unbracket(s2);
		return (s1.indexOf(s2) != -1) ? true : false;
	}

	/**
	 * 
	 * @param t2
	 *            substring
	 * @param t1
	 *            string
	 * @return substring that is before t1 in t2 append(Pre,[t2|_],t1) return only
	 *         first occurence
	 */
	public Term substring_before_2(Term t1, Term t2) {
		if ((t1.isAtom() == false) || (t2.isAtom() == false))
			return null;
		if ((t1.isList()) || (t2.isList()))
			return null;
		Term result = null;
		String s1 = t1.getTerm().toString();
		s1 = unbracket(s1);
		String s2 = t2.getTerm().toString();
		s2 = unbracket(s2);
		if (s1.indexOf(s2) != -1) {
			result = new Struct(s1.substring(0, s1.indexOf(s2)));
		}
		return result;
	}

	/**
	 * 
	 * @param t2
	 *            substring
	 * @param t1
	 *            string
	 * @return substring that is after t1 in t2 append(_,[t2|Post],t1) return only
	 *         first occurence
	 */
	public Term substring_after_2(Term t1, Term t2) {
		if ((t1.isAtom() == false) || (t2.isAtom() == false))
			return null;
		if ((t1.isList()) || (t2.isList()))
			return null;
		Term result = null;
		String s1 = t1.getTerm().toString();
		s1 = unbracket(s1);
		String s2 = t2.getTerm().toString();
		s2 = unbracket(s2);
		if (s1.indexOf(s2) != -1) {
			String temp = s1.substring(s1.indexOf(s2) + s2.length(), s1.length());
			result = new Struct(temp);
		}
		return result;
	}

	/**
	 * 
	 * @param t1
	 *            string
	 * @param start,
	 *            starting index
	 * @param end,
	 *            ending index
	 * @return t1=[...,c1_{start},c2_{start+1},...,cn_{end},...] => return t2 =
	 *         [c1,c2,...,cn] c1 at position 'start' in t1
	 */
	public Term substring_3(Term t1, Term start, Term end) {
		String s1 = t1.getTerm().toString();
		s1 = unbracket(s1);

		if ((start.isNumber() == false) || (end.isNumber() == false))
			return null;

		int starting = ((Number) start).intValue();
		int ending = ((Number) end).intValue();

		if (starting > ending)
			return null;

		if (t1.isAtom()) {
			int i1 = starting - 1;
			int i2 = ending;
			String temp = s1.substring(i1, i2);
			return new Struct(temp);
		}
		return null;
	}

	/**
	 * 
	 * @param t1
	 *            string
	 * @param start,
	 *            starting index
	 * @return substring starting at given index, ending with string end
	 * 
	 */
	public Term substring_2(Term t1, Term start) {
		Number position = (Number) start;
		Int i = new Int(t1.getTerm().toString().length());
		return substring_3(t1, position, i);
	}

	/**
	 * 
	 * @param t1
	 *            string
	 * @return length of string t1
	 */
	public Term string_length_1(Term t1) {
		if (t1.isList())
			return null;
		if (t1.isCompound())
			return null;
		if (t1.isNumber())
			return null;
		if (t1.isVar())
			return null;
		if (t1.isGround() == false)
			return null;
		if (t1.isAtom()) {
			String s1 = t1.getTerm().toString();
			s1 = unbracket(s1);
			int i = s1.length();
			return new Int(i);
		}
		return null;
	}

	/**
	 * 
	 * @param t1
	 *            string
	 * @return string without space at the beginning and at the end
	 *
	 */
	public Term normalize_space_1(Term t1) {
		if (t1.isList())
			return null;
		if (t1.isAtom()) {
			String s1 = t1.getTerm().toString();
			s1 = unbracket(s1);
			s1 = s1.trim();
			return new Struct(s1);
		}
		return null;
	}

	/**
	 * 
	 * @param t1
	 *            string (t1='hallo world')
	 * @param src
	 *            string to be replaced (src='world')
	 * @param dest
	 *            replaced string (dest='you')
	 * @return string where all occurences of src are replaced by dest (res='hallo
	 *         you')
	 * 
	 */
	public Term translate_3(Term term, Term src, Term dest) {
		if ((term.isList()) || (src.isList()) || (dest.isList()))
			return null;
		if ((term.isAtom()) && (src.isAtom()) && (dest.isAtom())) {
			String t1 = unbracket(term.getTerm().toString());
			String src1 = unbracket(src.getTerm().toString());
			String dest1 = unbracket(dest.getTerm().toString());

			String s1 = src1;
			String s2 = dest1;
			char[] cs1 = new char[1];
			char[] cs2 = new char[1];

			for (int i = 0; i < src1.length(); i++) {
				cs1[0] = src1.charAt(i);
				s1 = new String(cs1);

				if (i >= dest1.length()) {
					s2 = "";
				} else {
					cs2[0] = dest1.charAt(i);
					s2 = new String(cs2);
				}
				t1 = t1.replaceAll(s1, s2);
			}
			return new Struct(t1);
		}
		return null;
	}

	/**
	 * 
	 * @param t1
	 *            some 2p-term
	 * @return true, if t1 is a number false, otherwise
	 */
	public boolean isnumber_1(Term t1) {
		return (new ArithmeticTerm(t1)).isnumber();
	}

	/**
	 * 
	 * @param child
	 *            child-node (is subelement of children-list of tree)
	 * @param tree
	 *            top-node
	 * @return position of child in child-node-list in top-node tree
	 * 
	 *         after first occurence is found, further search will be terminated
	 */
	public Term position_2(Term child, Term tree) {
		TreeTerm treeTerm = new TreeTerm(tree);
		return treeTerm.position(child);
	}

	/**
	 * 
	 * @param t1
	 *            summand, e.g. element(aaa,[],[text('100')])
	 * @param t2
	 *            summand, e.g. element(bbb,[],[text('4')])
	 * @return sum of t1 and t2, e.g. '104'
	 */
	public Term plus_2(Term child, Term tree) {
		TreeTerm treeTerm = new TreeTerm(tree);
		return treeTerm.plus(child);
	}

	/**
	 * 
	 * @param t1
	 *            sum, e.g. element(aaa,[],[text('100')])
	 * @param t2
	 *            subtrahend, e.g. element(bbb,[],[text('4')])
	 * @return difference between t1 and t2, e.g. '96'
	 */
	public Term minus_2(Term child, Term tree) {
		TreeTerm treeTerm = new TreeTerm(tree);
		return treeTerm.minus(child);
	}

	/**
	 * 
	 * @param t1
	 *            factor, e.g. element(aaa,[],[text('100')])
	 * @param t2
	 *            factor, e.g. element(bbb,[],[text('4')])
	 * @return product of t1 and t2, e.g. '400'
	 */
	public Term mult_2(Term child, Term tree) {
		TreeTerm treeTerm = new TreeTerm(tree);
		return treeTerm.mult(child);
	}

	/**
	 * 
	 * @param t1
	 *            dividend, e.g. element(aaa,[],[text('100')])
	 * @param t2
	 *            divisor, e.g. element(bbb,[],[text('4')])
	 * @return quotient of t1 and t2, e.g. '25'
	 */
	public Term div_2(Term child, Term tree) {
		TreeTerm treeTerm = new TreeTerm(tree);
		return treeTerm.div(child);
	}

	/**
	 * 
	 * @param upcased,
	 *            upcased string (atom)
	 * @param normal,
	 *            string (can be upcased or not, is atom)
	 * @return true, if 'upcased' is the upcased string of 'normal' false, otherwise
	 *         upcase(upcased?,normal) => is not invertible
	 */
	public boolean upcase_2(Term upcased, Term normal) {
		if (normal.isAtom() == false) {
			return false;
		}
		String normalString = normal.getTerm().toString();
		normalString = unbracket(normalString);
		String upcasedString = normalString.toUpperCase();

		Term upcased2 = new Struct(upcasedString);

		if (upcased.unify(upcased2)) {
			return true;
		} else {
			return false;
		}
	}

	private String S1String;
	private String S1UpperString;
	private String S2String;
	private String S2UpperString;

	private String extractString(Term t1) {
		String s1 = t1.getTerm().toString();
		s1 = unbracket(s1);
		return s1;
	}

	private void checkStringExtraction(Term S1, Term S2) {
		S1String = extractString(S1);
		S1UpperString = S1String.toUpperCase();

		S2String = extractString(S2);
		S2UpperString = S2String.toUpperCase();
	}

	/**
	 * 
	 * @param S1
	 * @param S2
	 * 			@return, true if S1<_{upper_first} S2
	 * 
	 *            total ordering: cook < Czech < czech < TooK < Took < took
	 *            (ordering example taken from ZVON.org)
	 * 
	 */
	public boolean upper_first_2(Term S1, Term S2) {
		checkStringExtraction(S1, S2);

		if (S1UpperString.compareTo(S2UpperString) == 0) {
			if (S1String.compareTo(S2String) < 0) {
				return true;
			}
		} else if (S1UpperString.compareTo(S2UpperString) < 0) {
			return true;
		}
		return false;
	}

	/**
	 * 
	 * @param S1
	 * @param S2
	 * @return true if S1<_{lower_first} S2
	 * 
	 *         total ordering: cook < czech < Czech < took < Took < TooK
	 * 
	 */
	public boolean lower_first_2(Term S1, Term S2) {
		checkStringExtraction(S1, S2);

		if (S1UpperString.compareTo(S2UpperString) == 0) {
			if (S1String.compareTo(S2String) > 0) {
				return true;
			}
		} else if (S1UpperString.compareTo(S2UpperString) < 0) {
			return true;
		}
		return false;
	}

	/**
	 * 
	 * @param S1
	 * @param S2
	 * @return true if S1<_{first_upper} S2
	 * 
	 *         total ordering: Czech < TooK < Took < cook < czech < took
	 * 
	 */
	public boolean first_upper_2(Term S1, Term S2) {
		checkStringExtraction(S1, S2);

		if (S1String.compareTo(S2String) < 0) {
			return true;
		}
		return false;
	}

	/**
	 * 
	 * @param S1
	 * @param S2
	 * @return true if S1<_{first_upper} S2
	 * 
	 *         total ordering: cook < czech < took < Czech < TooK < Took
	 * 
	 */
	public boolean first_lower_2(Term S1, Term S2) {
		char[] c1 = (unbracket(S1.getTerm().toString())).toCharArray();
		char[] c2 = (unbracket(S2.getTerm().toString())).toCharArray();

		for (int i = 0; i < Math.min(c1.length, c2.length); i++) {
			boolean b1 = (c1[i] >= 'a') && (c1[i] <= 'z');
			boolean b2 = (c2[i] >= 'a') && (c2[i] <= 'z');
			// k g => true
			if (b1 && !b2) {
				return true;
			}
			// g k => false
			if (!b1 && b2) {
				return false;
			}
			// k k => '<' normal
			// g g => '<' normal
			else {
				if (c1[i] < c2[i]) {
					return true;
				}
				if (c1[i] > c2[i]) {
					return false;
				}
			}
		}
		return false;
	}

	public static final long serialVersionUID = 1;
}
