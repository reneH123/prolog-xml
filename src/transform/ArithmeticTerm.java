// Author: René Haberland
// 30.12.2018, Saint Petersburg, Russia
//  licensed under Creative Commons 4.0 Share Alike (CC BY-SA 4.0)
//  For more informations on the license terms, visit https://creativecommons.org
package transform;

import alice.tuprolog.Term;

public class ArithmeticTerm {
	private Term tree;

	public ArithmeticTerm() {
		tree = null;
	}

	public ArithmeticTerm(Term tree) {
		this.tree = tree;
	}

	private String unbracket(String in) {
		String out = in;
		if (in.charAt(0) == '\'') {
			out = in.substring(1, in.length() - 1);
		}
		return out;
	}

	public Term fnumber() {
		Term result = null;
		if (tree.isAtom()) {
			String s1 = tree.getTerm().toString();
			s1 = unbracket(s1);
			java.lang.Float F = java.lang.Float.valueOf(s1);
			result = new alice.tuprolog.Float(F.floatValue());
		}
		return result;
	}

	public Term inumber() {
		Term result = null;
		if (tree.isAtom()) {
			String s1 = tree.getTerm().toString();
			s1 = unbracket(s1);
			@SuppressWarnings( "deprecation" )
			Integer I = new Integer(s1);
			result = new alice.tuprolog.Int(I.intValue());
		}
		return result;
	}
	
	public boolean isnumber(){
		return tree.isNumber();
	}
}
