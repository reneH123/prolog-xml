// Author: Rene Haberland
// 30.12.2018, Saint Petersburg, Russia
//  licensed under Creative Commons 4.0 Share Alike (CC BY-SA 4.0)
//  For more informations on the license terms, visit https://creativecommons.org
package serializer;

import java.util.Comparator;
import java.util.Iterator;
import java.util.Stack;
import java.util.TreeSet;

import alice.tuprolog.Struct;
import alice.tuprolog.Term;
import alice.tuprolog.Var;

public class CanonicalVisitor extends DocumentPrepare implements Visitor {

	private class ReverseStringComparator implements Comparator {
		public int compare(Object o1, Object o2) {
			return ((String) o2).compareTo((String) o1);
		}
	}

	private PrologTermNode canonicalTerm;

	public CanonicalVisitor() {
		canonicalTerm = new PrologTermNode();
	}

	public PrologTermNode getCanonicalTerm() {
		return canonicalTerm;
	}

	public void visit(PrologTermNode myNode) {
		canonicalTerm.setStructReference(myVisit(myNode.getStructReference()));
	}

	private Struct canonizeAttributes(Struct t) {
		// insert sort
		@SuppressWarnings("unchecked")
		TreeSet<String> s = new TreeSet<String>(
				(Comparator<? super String>) new ReverseStringComparator());
		Iterator it = t.listIterator();
		while (it.hasNext()) {
			s.add(((Struct) it.next()).toString());
		}

		// reconstruct sorted attributes list
		Struct result = new Struct();
		it = s.iterator();
		while (it.hasNext()) {
			result = new Struct(new Struct(unbracket((String) it.next())),result);
		}
		return result;
	}

	private Struct visitChildren(Term topNode) {
		Struct root = new Struct();
		if (topNode.equals(new Struct()) == false) {
			Iterator it = ((Struct) topNode.getTerm()).listIterator();
			Stack<Struct> termList = new Stack<Struct>();
			while (it.hasNext()) {
				termList.push(myVisit((Struct) it.next()));
			}
			// iterative re-setting of the structure
			while (termList.isEmpty() == false) {
				Struct tmp = (Struct)termList.pop();
				root = new Struct(tmp, root);
			}
		}
		return root;
	}

	public Struct myVisit(Struct node) {
		Struct root = new Struct();
		Term Attributlist = null, Kinderknoten = null;
		Var v_A = new Var(), v_K = new Var(), v_N = new Var();

		// I) element(Name,AttributeList,KidNodes)
		if (node.match(new Struct("element", new Var(), new Var(), new Var()))) {
			node.unify(new Struct("element", v_N, v_A, v_K));
			Attributlist = v_A.getTerm();
			Kinderknoten = v_K.getTerm();
			// canonise each kid node
			root = visitChildren(Kinderknoten);

			return new Struct("element", v_N,
					canonizeAttributes((Struct) Attributlist), root);
		}
		// II) text(Name)
		else {
			return node;
		}
	}
}
