// Author: René Haberland
// 30.12.2018, Saint Petersburg, Russia
//  licensed under Creative Commons 4.0 Share Alike (CC BY-SA 4.0)
//  For more informations on the license terms, visit https://creativecommons.org
package serializer;

import alice.tuprolog.Struct;

// Decorator
/**
 * visitor-extension of alice.tuprolog.Struct
 */
public class PrologTermNode extends Struct {
	private Struct state;

	public Struct getStructReference() {
		return state;
	}

	public void setStructReference(Struct state) {
		this.state = state;
	}

	public void accept(Visitor v) {
		v.visit(this);
	}

	public static final long serialVersionUID = 1;
}
