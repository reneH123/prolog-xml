// Author: Rene Haberland
// 30.12.2018, Saint Petersburg, Russia
//  licensed under Creative Commons 4.0 Share Alike (CC BY-SA 4.0)
//  For more informations on the license terms, visit https://creativecommons.org
package serializer;

import java.io.FileWriter;
import java.util.Iterator;

import org.apache.xml.serialize.OutputFormat;
import org.apache.xml.serialize.XMLSerializer;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import alice.tuprolog.Struct;
import alice.tuprolog.Term;
import alice.tuprolog.Var;


public class XMLVisitor extends DocumentPrepare implements Visitor {

	public XMLVisitor(String targetFile) {
		prepareVisitor(targetFile);
	}

	// writes back to an XML-file an un-serialised DOM
	public void visit(PrologTermNode myNode) {
		Node top = myVisit(myNode);
		document.appendChild(top);
		try {
			OutputFormat format = new OutputFormat(document);
			format.setIndent(5);
			format.setEncoding("UTF-8");
			XMLSerializer serializer = new XMLSerializer(new FileWriter(
					getTargetFile()), format);
			serializer.asDOMSerializer();
			serializer.serialize((Element) top);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public Node myVisit(PrologTermNode myNode) {
		Struct node = myNode.getStructReference();
		Term Attributlist = null, Kinderknoten = null, Name = null;
		Var v_A = new Var(), v_K = new Var(), v_N = new Var();
		Node root = null;

		// I) element(Name,AttributeList,KidNodes)
		if (node.match(new Struct("element", new Var(), new Var(), new Var()))) {
			node.unify(new Struct("element", v_N, v_A, v_K));
			Attributlist = v_A.getTerm();
			Kinderknoten = v_K.getTerm();
			Name = v_N.getTerm();
			String SName = Name.toString();
			SName = unbracket(SName);

			root = document.createElement(SName);
			// append attribute
			Iterator it = ((Struct) Attributlist).listIterator();
			while (it.hasNext()) {
				Term t = (Struct) it.next();
				String string = unbracket(t.toString());
				String name = getAtt(string);
				String symbol = getSym(string);
				((Element) root).setAttribute(name, symbol);
			}
			// if kid nodes not empty
			if (Kinderknoten.equals(new Struct()) == false) {
				Iterator it2 = ((Struct) Kinderknoten.getTerm()).listIterator();
				while (it2.hasNext()) {
					PrologTermNode child = new PrologTermNode();
					child.setStructReference((Struct) it2.next());
					Node sub = myVisit(child);
					root.appendChild(sub);
				}
			}
		} // II) text(Name)
		else if (node.match(new Struct("text", new Var()))) {
			node.unify(new Struct("text", v_N));
			root = document.createTextNode(unbracket(v_N.getTerm().toString()));
		}
		// III) []
		else if (node.match(new Struct())) {
			// do nothing
		} else {
			new Exception("malformed structure in XMLVisitor.visit parsed!!!");
		}
		return root;
	}
}
