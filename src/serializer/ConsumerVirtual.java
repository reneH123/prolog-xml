// Author: René Haberland
// 30.12.2018, Saint Petersburg, Russia
//  licensed under Creative Commons 4.0 Share Alike (CC BY-SA 4.0)
//  For more informations on the license terms, visit https://creativecommons.org
package serializer;

public class ConsumerVirtual extends Consumer {
	private String s;

	public ConsumerVirtual() {
		s = "";
	}

	public void consume(Object s) {
		this.s = s.toString();
	}

	public Object getLast() {
		return s;
	}
}
