// Author: Rene Haberland
// 30.12.2018, Saint Petersburg, Russia
//  licensed under Creative Commons 4.0 Share Alike (CC BY-SA 4.0)
//  For more informations on the license terms, visit https://creativecommons.org
package serializer;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.xerces.jaxp.DocumentBuilderFactoryImpl;
import org.w3c.dom.Document;

/**
 * 
 * Toolbox for DOM-based operations
 * 
 */
public abstract class DocumentPrepare {
	private DocumentBuilderFactory factory;

	private DocumentBuilder builder;

	protected Document document;

	private String soureFile = "";

	private String targetFile = "";

	public String getSoureFile() {
		return soureFile;
	}

	public String getTargetFile() {
		return targetFile;
	}

	// '..' => ..
	protected String unbracket(String in) {
		String s;
		if (in.startsWith("'"))
			s = in.substring(1, in.length() - 1);
		else
			s = in;
		return s;
	}

	// att="sym" => att
	protected String getAtt(String s) {
		int i = s.indexOf('=');
		return s.substring(0, i);
	}

	// att="sym" => sym
	protected String getSym(String s) {
		int i = s.indexOf('=');
		return s.substring(i + 2, s.length() - 1);
	}

	protected boolean prepareParser(String sourceFile) {
		try {
			factory = new DocumentBuilderFactoryImpl();
			builder = factory.newDocumentBuilder();
			this.soureFile = sourceFile;
			document = builder.parse(sourceFile);
			return true;
		} catch (Exception e) {
			System.err.print("DEBUG: ");
			e.printStackTrace();
			return false;
		}
	}

	protected boolean prepareVisitor(String targetFile) {
		try {
			factory = DocumentBuilderFactory.newInstance();
			builder = factory.newDocumentBuilder();
			document = builder.newDocument();
			if (targetFile == null)
				this.targetFile = "output.xml";
			else
				this.targetFile = targetFile;
			return true;
		} catch (Exception e) {
			return false;
		}

	}
}
