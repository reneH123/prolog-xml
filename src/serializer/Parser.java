// Author: Rene Haberland
// 30.12.2018, Saint Petersburg, Russia
//  licensed under Creative Commons 4.0 Share Alike (CC BY-SA 4.0)
//  For more informations on the license terms, visit https://creativecommons.org
package serializer;

import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import alice.tuprolog.Struct;

/**
 * 
 * @author rene
 *  * recursive descendand parser (via DOM)
 * 
 * recognises the language with the following grammar:
 * 
 * G = (VN,VT,PROLOGTERM,P)
 * 
 * VN = {PROLOGTERM, ATTLIST, CHILDNODES}
 * VT = {(,),,,[,],ID,TEXT,element,text}
 * P:
 * 
 * PROLOGTERM -> element ( ID , ATTLIST , CHILDNODES )
 * PROLOGTERM -> text ( ' TEXT ' )
 * ATTLIST    -> [ ] 
 * ATTLIST    -> [ ' ID = TEXT ' ATTLIST2 ]
 * ATTLIST2   -> 
 * ATTLIST2   -> , ' ID = TEXT ' ATTLIST2
 * CHILDNODES -> [ ]
 * CHILDNODES -> [ PROLOGTERM CHILDNODES2 ]
 * CHILDNODES2->
 * CHILDNODES2-> , PROLOGTERM CHILDNODES2
 * 
 */
public class Parser extends DocumentPrepare{
	
	public Parser(String sourceFile) {
		prepareParser(sourceFile);
	}
	
	public void execute(Consumer consumer) {
		Struct node = PrologTerm(document.getFirstChild());
		consumer.consume(node); 
	}
	
	/**
	 * @param document
	 * @return [] | [ ' ID = TEXT ' { , ' ID = TEXT ' } ]
	 */
	public Struct AttributesList(Node topNode) {
		NamedNodeMap attributes = topNode.getAttributes();
		if (attributes == null)
			return new Struct();
		Node node = null;
		Struct result=new Struct();
		for (int i = 0; i < attributes.getLength(); i++) {
			node = attributes.item(i);
			String s=node.getNodeName() + "=\"" + node.getTextContent() + "\"";
			result=new Struct(new Struct(s),result);
		}
		return result;
	}
	/**
	 * @param document
	 * @return [] | [ PROLOGTERM { , PROLOGTERM } ]
	 */
	public Struct ChildrenList(Node topNode) {
		NodeList nodes = topNode.getChildNodes();
		if (nodes == null)
			return new Struct();
		Struct result=new Struct();
		Node node = null;
		
		Struct currentNode=null;
		for (int i = nodes.getLength()-1; i>=0 ; i--) {
			node = nodes.item(i);
			currentNode = PrologTerm(node);
			// filter empty nodes
			if (currentNode!=null){
				result = new Struct(PrologTerm(node),result);
			}
		}
		return result;
	}

	/**
	 * @param document
	 * @return element ( ID , ATTLIST , CHILDNODES )  |
	 * 		   text ( ' TEXT ' ), if TEXT starts with uppercase letter
	 *         text ( TEXT ),  if TEXT starts with lowercase letter
	 */
	public Struct PrologTerm(Node topNode) {
		String nodeName = null;
		Struct s = null;
		switch (topNode.getNodeType()) {
		case Node.ELEMENT_NODE:
			nodeName = topNode.getNodeName();			
			Struct attlist = AttributesList(topNode);
			Struct childnodes = ChildrenList(topNode);
			s = new Struct("element",new Struct(nodeName),attlist,childnodes);
			break;
		case Node.TEXT_NODE:
			nodeName = topNode.getTextContent();//getNodeValue();
			nodeName = nodeName.trim();
			if (!nodeName.equals(""))
				s = new Struct("text",new Struct(nodeName));
			break;
		case Node.COMMENT_NODE:
			nodeName = "%";
			break;
		case Node.PROCESSING_INSTRUCTION_NODE:
			nodeName = "?";
			break;
		}
		return s;
	}

}
