// Author: René Haberland
// 30.12.2018, Saint Petersburg, Russia
//  licensed under Creative Commons 4.0 Share Alike (CC BY-SA 4.0)
//  For more informations on the license terms, visit https://creativecommons.org
package serializer;

public class ConsumerConsole extends Consumer {

	public void consume(Object s) {
		System.out.println(s);
	}

	public Object getLast() {
		return null;
	}
}
