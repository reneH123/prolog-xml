// Author: Ren� Haberland
// 30.12.2018, Saint Petersburg, Russia
//  licensed under Creative Commons 4.0 Share Alike (CC BY-SA 4.0)
//  For more informations on the license terms, visit https://creativecommons.org
/**
 * modified SimpleTransformer-examples as originally provided by Xalan
 * 
 *  call: SimpleTransformer foo
 *  prerequisites: foo.xml, foo.xsl exist
 */
package xslt;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

public class SimpleTransformer {
	public static void err(String msg) {
		System.err.println(msg);
	}

	public static void main(String[] args) throws TransformerException,
			TransformerConfigurationException, FileNotFoundException,
			IOException {
		if (args.length != 1) {
			err("Usage: SimpleTransformer filename");
			err("  There must be a file named filename.xml and filename.xsl.");
			err("  At the end you get a file named filename2.xml");
			System.exit(1);
		}

		TransformerFactory tFactory = TransformerFactory.newInstance();
		Transformer transformer = tFactory.newTransformer(new StreamSource(
				args[0] + ".xsl"));
		transformer.transform(new StreamSource(args[0] + ".xml"),
				new StreamResult(new FileOutputStream(args[0] + "2.xml")));
	}

}
