// Author: René Haberland
// 30.12.2018, Saint Petersburg, Russia
//  licensed under Creative Commons 4.0 Share Alike (CC BY-SA 4.0)
//  For more informations on the license terms, visit https://creativecommons.org
package xslt;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

public class SimpleTransform {

	public static void main(String[] args) throws TransformerException,
			TransformerConfigurationException, FileNotFoundException,
			IOException {
		if (args.length != 3) {
			System.out.println(":>java SimpleTransform xmlin xsl xmlout");
			System.exit(-1);
		}
		System.out.println("SimpleTransform " + args[0] + " " + args[1] + " "
				+ args[2]);
		TransformerFactory tFactory = TransformerFactory.newInstance();

		Transformer transformer = tFactory.newTransformer(new StreamSource(
				args[1]));

		transformer.transform(new StreamSource(args[0]), new StreamResult(
				new FileOutputStream(args[2])));
		System.out.println("...ready!");

	}

}
