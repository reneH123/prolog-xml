 COPYRIGHT notification
Author: René Haberland, 30.12.2018, Saint Petersburg, Russia

All software, statistics, samples containing in this folder are licensed under Creative Commons 4.0 Share Alike (CC BY-SA 4.0), applicable wordwide.


The software had been discussed and contributed to the AMICT08 (8th International Conference on Advanced Methods in Information and Communication Technology) conference proceedings, and published since then in conference articles.
  LEGACY LICENSING: In case you personally were granted rights to use the software before the new CC4.0.SA license, this was always only under the condition of naming the author and for non-commercial purposes only - as you were definetely informed about it.


3RD PARTY SOFTWARE USING:
 - Apache Xalan, Apache License
 - tuProlog (University of Bologna, Italy), GPL version 3
 - Samples were in the years 2007/2008 taken from ZVON.org (to that time under PUBLIC DOMAIN)



This software is related to:

@misc{haberl2019using,
    title={Using Prolog for Transforming XML-Documents},
    author={René Haberland},
    year={2019},
    eprint={1912.10817},
    archivePrefix={arXiv},
    primaryClass={cs.PL}
}

@misc{haberl2019transformation,
    title={Transformation of XML Documents with Prolog},
    author={René Haberland and Igor L. Bratchikov},
    year={2019},
    eprint={1906.08361},
    archivePrefix={arXiv},
    primaryClass={cs.LO}
}
