// Author: René Haberland
// 30.12.2018, Saint Petersburg, Russia
//  licensed under Creative Commons 4.0 Share Alike (CC BY-SA 4.0)
//  For more informations on the license terms, visit https://creativecommons.org
import java.util.Iterator;

public class XMLVisitor extends Visitor {
	public static void writeln(String s){
		System.out.println(s);
	}
	public static void write(String s){
		System.out.print(s);
	}
	
	public void printXMLAttribute(Attribute a){
		write(a.name+"=\""+a.value+"\"");
	}
	
	public void printXMLAttributes(Aggregate aggr){
		Iterator it = aggr.aggregate.iterator();
		while (it.hasNext()){
			Attribute att = (Attribute)it.next();
			write(" ");
			printXMLAttribute(att);
		}
	}
	
	public void printXMLChildren(Aggregate aggr){
		Iterator it = aggr.aggregate.iterator();
		while (it.hasNext()){
			Node node = (Node)it.next();
			node.accept(this);
		}
	}
	
	public void visitElementNode(ElementNode node) {
		String nodeName = node.getID();
		Aggregate attributes = node.getATTLIST();
		Aggregate children  = node.getCHILDNODES();
		
		writeln("<"+nodeName);
		printXMLAttributes(attributes);
		writeln(">");
		printXMLChildren(children);
		writeln("</"+nodeName+">");
	}

	public void visitTextNode(TextNode node) {
		write(node.getTextName());
	}
}
