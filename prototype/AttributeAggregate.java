// Author: Ren� Haberland
// 30.12.2018, Saint Petersburg, Russia
//  licensed under Creative Commons 4.0 Share Alike (CC BY-SA 4.0)
//  For more informations on the license terms, visit https://creativecommons.org
import java.util.Iterator;
import java.util.LinkedList;

public class AttributeAggregate extends Aggregate {
	public AttributeAggregate() {
		aggregate = new LinkedList<Attribute>();
	}
	
	
	/**
	 * @return [ ' ID = TEXT ' {� , ' ID = TEXT ' }� ]
	 * 
	 */
	public String toString() {
		String subTerm;
		Iterator it = aggregate.iterator();
		subTerm = "[";
		if (it.hasNext()==false) 
			return "[]";
		Attribute attribute = (Attribute) it.next();
		subTerm += attribute.toString();
		while (it.hasNext()) {
			attribute = (Attribute) it.next();
			subTerm += ", " + attribute.toString();
		}
		subTerm += "]";
		return subTerm;
	}
}
