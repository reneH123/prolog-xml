// Author: René Haberland
// 30.12.2018, Saint Petersburg, Russia
//  licensed under Creative Commons 4.0 Share Alike (CC BY-SA 4.0)
//  For more informations on the license terms, visit https://creativecommons.org
public class Attribute {
	public String name;

	public String value;
	
	public Attribute(String name, String value){
		this.name = name;
		this.value = value;
	}

	/**
	 * @return name=value
	 */
	public String toString() {
		return "'" + name + "=" + value + "'";
	}
}
