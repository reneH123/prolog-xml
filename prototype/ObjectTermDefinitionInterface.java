// Author: René Haberland
// 30.12.2018, Saint Petersburg, Russia
//  licensed under Creative Commons 4.0 Share Alike (CC BY-SA 4.0)
//  For more informations on the license terms, visit https://creativecommons.org
import org.w3c.dom.Node;

public interface ObjectTermDefinitionInterface {
	public Aggregate ChildrenList(Node topNode);
	public Aggregate AttributesList(Node topNode);
	public objectparser.Node PrologTerm(Node topNode);
}
