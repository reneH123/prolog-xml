// Author: Ren� Haberland
// 30.12.2018, Saint Petersburg, Russia
//  licensed under Creative Commons 4.0 Share Alike (CC BY-SA 4.0)
//  For more informations on the license terms, visit https://creativecommons.org
import java.util.Iterator;
import java.util.LinkedList;

public class NodeAggregate extends Aggregate {
	public NodeAggregate() {
		aggregate = new LinkedList<Node>();
	}

	/**
	 * @return [ PROLOGTERM {� , PROLOGTERM }� ]
	 */
	public String toString() {
		String subTerm;
		Iterator it = aggregate.iterator();
		subTerm = "[";
		if (it.hasNext()==false)
			return "[]";
		subTerm += ((Node) it.next()).toString();
		while (it.hasNext()) {
			Node node = (Node) it.next();
			subTerm += ", " + node.toString();
		}
		subTerm += "]";
		return subTerm;
	}
}
