// Author: René Haberland
// 30.12.2018, Saint Petersburg, Russia
//  licensed under Creative Commons 4.0 Share Alike (CC BY-SA 4.0)
//  For more informations on the license terms, visit https://creativecommons.org
/**
 * 
 * @author rene implicit language representation of a XML-textnode
 * 
 */
public class TextNode extends Node {
	private String TEXT;

	public TextNode(String textName) {
		setTextName(textName);
	}

	public String getTextName() {
		return TEXT;
	}

	public void setTextName(String textName) {
		this.TEXT = textName;
	}

	/**
	 * @return text ( ' TEXT ' )
	 */
	public String toString() {
		return "text('" + getTextName() + "')";
	}
	
	public void accept(Visitor v) {
		v.visitTextNode(this);
	}
}
