// Author: Rene Haberland
// 30.12.2018, Saint Petersburg, Russia
//  licensed under Creative Commons 4.0 Share Alike (CC BY-SA 4.0)
//  For more informations on the license terms, visit https://creativecommons.org
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 * 
 * @author Rene Haberland
 * 
 * recognises the language with the following grammar:
 * 
 * G = (VN,VT,PROLOGTERM,P)
 * 
 * VN = {PROLOGTERM, ATTLIST, CHILDNODES}
 * VT = {(,),,,[,],ID,TEXT,element,text}
 * P:
 * 
 * PROLOGTERM -> element ( ID , ATTLIST , CHILDNODES )
 * PROLOGTERM -> text ( ' TEXT ' )
 * ATTLIST    -> [ ] 
 * ATTLIST    -> [ ' ID = TEXT ' ATTLIST2 ]
 * ATTLIST2   -> 
 * ATTLIST2   -> , ' ID = TEXT ' ATTLIST2
 * CHILDNODES -> [ ]
 * CHILDNODES -> [ PROLOGTERM CHILDNODES2 ]
 * CHILDNODES2->
 * CHILDNODES2-> , PROLOGTERM CHILDNODES2
 * 
 */
public class Parser2 extends ObjectParser {
	public Parser2(String sourceFile) {
		super(sourceFile);
	}
	/**
	 * @param document
	 * @return [] | [ ' ID = TEXT ' { , ' ID = TEXT ' } ]
	 */
	public Aggregate AttributesList(Node topNode) {
		NamedNodeMap attributes = topNode.getAttributes();
		if (attributes == null)
			return new EmptyAggregate();
		Node node = null;
		Aggregate result=new AttributeAggregate();
		for (int i = 0; i < attributes.getLength(); i++) {
			node = attributes.item(i);
			result.aggregate.add(new Attribute(node.getNodeName(),node.getTextContent()));
		}
		return result;
	}
	/**
	 * @param document
	 * @return [] | [ PROLOGTERM { , PROLOGTERM } ]
	 */
	public Aggregate ChildrenList(Node topNode) {
		NodeList nodes = topNode.getChildNodes();
		if (nodes == null)
			return new EmptyAggregate();
		Aggregate result=new NodeAggregate();
		Node node = null;
		
		objectparser.Node currentNode=null;
		for (int i = 0; i < nodes.getLength(); i++) {
			node = nodes.item(i);
			currentNode = PrologTerm(node);
			// filter empty nodes
			//if (currentNode.equals("")==false){
			if (currentNode!=null){
				result.aggregate.add(PrologTerm(node));
			}
		}
		return result;
	}

	/**
	 * @param document
	 * @return element ( ID , ATTLIST , CHILDNODES )  |
	 * 		   text ( ' TEXT ' )
	 */
	public objectparser.Node PrologTerm(Node topNode) {
		String nodeName = null;
		objectparser.Node s=null;
		switch (topNode.getNodeType()) {
		case Node.ELEMENT_NODE:
			nodeName = topNode.getNodeName();			
			Aggregate attlist = AttributesList(topNode);
			Aggregate childnodes = ChildrenList(topNode);
			s = new ElementNode(nodeName,attlist,childnodes);
			break;
		case Node.TEXT_NODE:
			nodeName = topNode.getNodeValue();
			nodeName = nodeName.trim();
			if (!nodeName.equals(""))
				s = new TextNode(nodeName);
			break;
		case Node.COMMENT_NODE:
			nodeName = "%";
			break;
		case Node.PROCESSING_INSTRUCTION_NODE:
			nodeName = "?";
			break;
		}
		return s;
	}

}
