// Author: René Haberland
// 30.12.2018, Saint Petersburg, Russia
//  licensed under Creative Commons 4.0 Share Alike (CC BY-SA 4.0)
//  For more informations on the license terms, visit https://creativecommons.org
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

import com.sun.org.apache.xerces.internal.jaxp.DocumentBuilderFactoryImpl;

public abstract class ObjectParser implements ObjectTermDefinitionInterface {
	private DocumentBuilderFactory factory;

	private DocumentBuilder builder;

	private Document document;

	
	public void execute(Consumer consumer) {
		Node node = PrologTerm(document.getFirstChild());
		node.accept(new XMLVisitor());
		consumer.consume(node); 
	}

	protected ObjectParser(String sourceFile) {
		try {
			factory = new DocumentBuilderFactoryImpl();
			builder = factory.newDocumentBuilder();
			document = builder.parse(sourceFile);
		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			e.printStackTrace();
		}
	}
}
