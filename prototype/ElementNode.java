// Author: René Haberland
// 30.12.2018, Saint Petersburg, Russia
//  licensed under Creative Commons 4.0 Share Alike (CC BY-SA 4.0)
//  For more informations on the license terms, visit https://creativecommons.org
public class ElementNode extends Node {
	private String ID;

	private Aggregate ATTLIST;

	private Aggregate CHILDNODES;

	public ElementNode(String ID, Aggregate ATTLIST, Aggregate CHILDNODES) {
		this.ID = ID;
		this.ATTLIST = ATTLIST;
		this.CHILDNODES = CHILDNODES;
	}

	
	public Aggregate getATTLIST() {
		return ATTLIST;
	}


	public void setATTLIST(Aggregate attlist) {
		ATTLIST = attlist;
	}


	public Aggregate getCHILDNODES() {
		return CHILDNODES;
	}


	public void setCHILDNODES(Aggregate childnodes) {
		CHILDNODES = childnodes;
	}


	public String getID() {
		return ID;
	}


	public void setID(String id) {
		ID = id;
	}


	/**
	 * @return element ( ID , ATTLIST , CHILDNODES )
	 */
	public String toString() {
		return "element(" + ID + "," + ATTLIST.toString() + ","
				+ CHILDNODES.toString() + ")";
	}
	
	public void accept(Visitor v) {
		v.visitElementNode(this);
	}
}
